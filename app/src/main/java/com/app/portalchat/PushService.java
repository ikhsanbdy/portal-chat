package com.app.portalchat;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

/**
 * Created by Ikhsan on 3/8/2015.
 */
public class PushService extends Service {

    public static final String PREFS_NAME = "PortalChatPrefs";

    static final int NEW_MESSAGE = 1;
    static final int SENT_MESSAGE = 2;
    static final int SEND_MESSAGE = 3;
    static final int REGISTER_CLIENT = 4;
    static final int UNREGISTER_CLIENT = 5;
    private static boolean mIsRunning = false;
    final Messenger mMessenger = new Messenger(new incomingHandler());
    private String mUsername;
    private String mToken;
    private DBController dbController;
    private Socket mSocket;
    private String mCourse = "";
    private String mClass = "";
    private Messenger mActivity = null;
    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            authUser();
        }
    };
    private Emitter.Listener onAuthResponse = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            authResponse(args[0].toString());
        }
    };
    private Emitter.Listener onSharePost = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            sharePost(args[0].toString());
        }
    };
    private Emitter.Listener onPostRespone = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            postRespone(args[0].toString());
        }
    };

    public static boolean isRunning() {
        return mIsRunning;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    @Override
    public void onCreate() {
        {
            try {
                mSocket = IO.socket("https://palawajs.herokuapp.com");
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }

        dbController = new DBController(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on("auth_response", onAuthResponse);
        mSocket.on("share_post", onSharePost);
        mSocket.on("post_response", onPostRespone);
        mSocket.connect();

        mIsRunning = true;

        return START_STICKY;
    }

    @Override
    public void onDestroy() {

        mSocket.disconnect();
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off("auth_response", onAuthResponse);
        mSocket.off("share_post", onSharePost);
        mSocket.off("post_response", onPostRespone);

        mIsRunning = false;

        super.onDestroy();
    }

    private boolean isActivityRunning(String courseId, String classId) {
        if (mCourse.equals(courseId) && mClass.equals(classId)) {
            return true;
        }
        return false;
    }

    private void authUser() {
        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, 0);
        mUsername = preferences.getString("username", "");
        mToken = preferences.getString("token", "");

        JSONObject object = new JSONObject();
        try {
            object.put("user", mUsername);
            object.put("token", mToken);
        } catch (JSONException e) {
            return;
        }

        mSocket.emit("auth_user", object.toString());
    }

    private void sendReceived(String messageId) {
        JSONObject object = new JSONObject();
        try {
            object.put("s_id", messageId);
        } catch (JSONException e) {
            return;
        }

        mSocket.emit("post_received", object.toString());
    }

    private void postRespone(String args) {
        String time;
        String sId;
        long cId;
        JSONObject object = null;
        try {
            object = new JSONObject(args);
            cId = object.getJSONObject("response").getLong("c_id");

            if (object.getString("status").equals("success")) {
                sId = object.getJSONObject("response").getString("s_id");
                time = object.getJSONObject("response").getString("time");
                dbController.updateSentPost(cId, sId, time);
            } else {
                time = "Failed, tap to retry";
                dbController.deleteUnsentPost(cId);
            }
        } catch (JSONException e) {
            return;
        }

        //send to activity
        if (mActivity != null) {
            try {
                Bundle bundle = new Bundle();
                bundle.putLong("c_id", cId);
                bundle.putString("time", time);
                Message sMessage = Message.obtain(null, SENT_MESSAGE);
                sMessage.setData(bundle);
                sMessage.replyTo = mMessenger;
                mActivity.send(sMessage);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }


    }

    private void sharePost(String args) {
        JSONObject object = null;
        String username;
        String message;
        String messageId;
        String time;
        String courseId;
        String classId;
        try {
            object = new JSONObject(args);
            username = object.getString("username");
            message = object.getString("message");
            time = object.getString("time");
            courseId = object.getString("course_id");
            classId = object.getString("class_id");
            messageId = object.getString("_id");
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        //send to activity
        int read = 0;
        if (mActivity != null && isActivityRunning(courseId, classId)) {
            try {
                Bundle bundle = new Bundle();
                bundle.putString("username", username);
                bundle.putString("message", message);
                bundle.putString("time", time);
                Message sMessage = Message.obtain(null, NEW_MESSAGE);
                sMessage.setData(bundle);
                sMessage.replyTo = mMessenger;
                mActivity.send(sMessage);
                read = 1;
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else {
            String text = username + " : " + message;
            displayNotification(0, courseId, classId, getString(R.string.app_name), text);
        }

        sendReceived(messageId);
        dbController.insertPost(object, read);
    }

    private void displayNotification(int nId, String courseId, String classId, String title, String text) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_chat_white)
                .setContentTitle(title)
                .setContentText(text)
                .setTicker(text)
                .setAutoCancel(true)
                .setVibrate(new long[]{0, 400, 80, 200, 50, 300})
                .setLights(0x0000FF, 2000, 1000);

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("course_id", courseId);
        intent.putExtra("class_id", classId);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(nId, mBuilder.build());
    }

    private void authResponse(String args) {
        String message;
        JSONObject object = null;
        try {
            object = new JSONObject();
            message = object.getJSONObject("response").getString("message");
        } catch (JSONException e) {
            return;
        }

        //send to activity

    }

    class incomingHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            Bundle bundle;
            switch (message.what) {
                case REGISTER_CLIENT:
                    mActivity = message.replyTo;
                    bundle = message.getData();
                    mCourse = bundle.getString("course_id");
                    mClass = bundle.getString("class_id");
                    break;
                case UNREGISTER_CLIENT:
                    mActivity = null;
                    mCourse = "";
                    mClass = "";
                    break;
                case SEND_MESSAGE:
                    bundle = message.getData();
                    sendMessage(bundle.getLong("c_id", 0), bundle.getString("course_id"), bundle.getString("class_id"), bundle.getString("message"));
                    break;
                default:
                    super.handleMessage(message);
            }
        }

        private void sendMessage(long cId, String courseId, String classId, String message) {
            if (!mSocket.connected()) {
                sendFailed(cId, "Failed, tap to retry");
                return;
            }

            JSONObject object = new JSONObject();
            try {
                object.put("course_id", courseId);
                object.put("class_id", classId);
                object.put("message", message);
                object.put("c_id", cId);
            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }

            mSocket.emit("share_post", object.toString());
        }

        private void sendFailed(long cId, String message) {
            try {
                Bundle bundle = new Bundle();
                bundle.putLong("c_id", cId);
                bundle.putString("time", message);
                Message sMessage = Message.obtain(null, SENT_MESSAGE);
                sMessage.setData(bundle);
                sMessage.replyTo = mMessenger;
                mActivity.send(sMessage);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
