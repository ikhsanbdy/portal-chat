package com.app.portalchat;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;


public class MainActivity extends ActionBarActivity {
    public static final String PREFS_NAME = "PortalChatPrefs";

    final Messenger mMessenger = new Messenger(new incomingHandler());

    private Messenger mService = null;
    private RecyclerView mMessagesView;
    private LinearLayoutManager mLayoutManager;
    private EditText mInputMessageView;
    private ImageButton sendButton;
    private List<ItemMessage> mMessages = new ArrayList<ItemMessage>();
    private RecyclerView.Adapter mAdapter;
    private String mUsername;
    private String mToken;
    private String mCourse;
    private String mClass;
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            try {
                Bundle bundle = new Bundle();
                bundle.putString("course_id", mCourse);
                bundle.putString("class_id", mClass);
                Message message = Message.obtain(null, PushService.REGISTER_CLIENT);
                message.setData(bundle);
                message.replyTo = mMessenger;
                mService.send(message);
            } catch (RemoteException e) {
                return;
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            mService = null;
        }
    };
    private int mLimit;
    private int mOffset;
    private int mPage;
    private boolean mLoading;
    private boolean mIsBound;
    private DBController dbController;
    private Intent intent;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbar();

        mLimit = 20;
        mOffset = 0;
        mPage = 1;

        initData();

        initMessagesView();

        mInputMessageView = (EditText) findViewById(R.id.message_input);
        setInputViewListener();

        sendButton = (ImageButton) findViewById(R.id.send_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptSend();
            }
        });

        mLoading = loadMessage();

        if (!isServiceRunning()) {
            intent = new Intent(this, PushService.class);
            this.startService(intent);
        }

        if (!mIsBound) {
            doBindService();
        }
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.app_name);
    }

    private void initData() {
        mUsername = "300001";
        mToken = "0987654321ytrewq0987654321ytrewq";
        mCourse = "DUMMY1234";
        mClass = "DUMMY-A-1234";

        //disimpan agar dapat diambil service untuk otentikasi ke server
        //kalo di sharedpreferens sudah tersimpan username dan token, nggak perlu disimpan lagi
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, 0).edit();
        editor.putString("username", mUsername);
        editor.putString("token", mToken);
        editor.commit();
    }

    private void initMessagesView() {
        mAdapter = new MessageAdapter(this, mMessages);
        dbController = new DBController(getApplicationContext());

        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setStackFromEnd(true);
        mMessagesView = (RecyclerView) findViewById(R.id.messages);
        mMessagesView.setHasFixedSize(true);
        mMessagesView.setLayoutManager(mLayoutManager);
        mMessagesView.setAdapter(mAdapter);
        setMessagesViewListener();
        setOnClickRecyclerView();
    }

    private void setOnClickRecyclerView() {
        final GestureDetector mGestureDetector = new GestureDetector(MainActivity.this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });

        mMessagesView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                    int position = recyclerView.getChildPosition(child);
                    resendFailedMessage(position);
                    return true;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
            }
        });
    }

    private void resendFailedMessage(int position) {
        ItemMessage message = mMessages.get(position);
        if (message != null && message.getTime().equals("Failed, tap to retry")) {
            long cId = message.getId();
            String msg = message.getMessage();
            mMessages.get(position).setTime("Sending");
            mAdapter.notifyItemChanged(position);
            sendMessageToService(cId, msg);
        }
    }

    private void doBindService() {
        bindService(new Intent(this, PushService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    private void doUnbindService() {
        if (mIsBound) {
            if (mService != null) {
                try {
                    Message message = Message.obtain(null, PushService.UNREGISTER_CLIENT);
                    message.replyTo = mMessenger;
                    mService.send(message);
                } catch (RemoteException e) {
                    return;
                }
            }
            unbindService(mConnection);
            mIsBound = false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        doUnbindService();
    }

    @Override
    protected void onResume() {
        super.onResume();
        doBindService();
        loadUnreadMessage();
    }

    @Override
    protected void onPause() {
        super.onPause();
        doUnbindService();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Activity.RESULT_OK != resultCode) {
            finish();
            return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_leave) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isServiceRunning() {
        if (PushService.isRunning()) {
            return true;
        }
        return false;
    }

    private void setMessagesViewListener() {
        mMessagesView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int pos = mLayoutManager.findFirstVisibleItemPosition();
                if (pos == 0 && mLoading) {
                    mLoading = loadMessage();
                }
            }
        });
    }

    private void setInputViewListener() {
        mInputMessageView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int id, KeyEvent event) {
                if (id == R.id.send || id == EditorInfo.IME_NULL) {
                    attemptSend();
                    return true;
                }
                return false;
            }
        });
    }

    private void addMessage(long id, String username, String message, String time, int type) {
        mMessages.add(new ItemMessage.Builder(type).id(id)
                .username(username).message(message).time(time).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        scrollToBottom();
    }

    private boolean loadMessage() {
        ArrayList<HashMap<String, String>> messageList = dbController.getMessages(mCourse, mClass, mLimit, mOffset);

        final int listSize = messageList.size();
        if (listSize > 0) {
            String username;
            String message;
            String time;
            int owner;
            long id;
            HashMap<String, String> map;
            for (int i = 0; i < listSize; i++) {
                map = messageList.get(i);
                username = map.get("username");
                message = map.get("message");
                try {
                    time = dateFormat(map.get("time"));
                } catch (ParseException e) {
                    time = "";
                }
                id = Long.valueOf(map.get("id"));

                owner = (username.equals(mUsername) ? ItemMessage.TYPE_MESSAGE_SELF : ItemMessage.TYPE_MESSAGE_OTHER);
                mMessages.add(0, new ItemMessage.Builder(owner).id(id)
                        .username(username).message(message).time(time).build());
            }

            mAdapter.notifyItemRangeInserted(0, listSize);
            mAdapter.notifyItemRangeChanged(listSize, mAdapter.getItemCount() - listSize);

            mOffset = mPage * mLimit;
            mPage++;

            dbController.updateReadPost(mCourse, mClass);

            return true;
        }

        return false;
    }

    private void loadUnreadMessage() {
        ArrayList<HashMap<String, String>> messageList = dbController.getUnreadMessages(mCourse, mClass);

        final int listSize = messageList.size();
        if (listSize > 0) {
            String username;
            String message;
            String time;
            long id;
            int owner;
            HashMap<String, String> map;
            for (int i = 0; i < listSize; i++) {
                map = messageList.get(i);
                username = map.get("username");
                message = map.get("message");
                id = Long.valueOf(map.get("id"));
                try {
                    time = dateFormat(map.get("time"));
                } catch (ParseException e) {
                    time = "";
                }

                owner = (username.equals(mUsername) ? ItemMessage.TYPE_MESSAGE_SELF : ItemMessage.TYPE_MESSAGE_OTHER);
                mMessages.add(new ItemMessage.Builder(owner).id(id)
                        .username(username).message(message).time(time).build());
            }

            mAdapter.notifyItemRangeInserted(mAdapter.getItemCount(), listSize - 1);
            dbController.updateReadPost(mCourse, mClass);
            scrollToBottom();
        }
    }


    private String dateFormat(String dateTime) throws ParseException {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        DateFormat outputFormat;
        Date parsed = inputFormat.parse(dateTime);
        Date now = new Date();
        long diff = (now.getTime() - parsed.getTime()) / (24 * 60 * 60 * 1000);
        if (diff < 7) {
            outputFormat = new SimpleDateFormat("EEE, HH:mm");
        } else {
            outputFormat = new SimpleDateFormat("MMM d, HH:mm");
        }
        return outputFormat.format(parsed);
    }

    private void attemptSend() {
        if (null == mUsername) return;

        String message = mInputMessageView.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            mInputMessageView.requestFocus();
            return;
        }

        mInputMessageView.setText("");

        JSONObject object = new JSONObject();
        try {
            object.put("_id", "");
            object.put("course_id", mCourse);
            object.put("class_id", mClass);
            object.put("message", message);
            object.put("username", mUsername);
            object.put("time", "");
        } catch (JSONException e) {
            return;
        }

        long cId = dbController.insertPost(object, 1);

        addMessage(cId, mUsername, message, "Sending", 0);

        //send to service
        sendMessageToService(cId, message);
    }

    private void sendMessageToService(long cId, String message) {
        if (mIsBound) {
            if (mService != null) {
                try {
                    Bundle bundle = new Bundle();
                    bundle.putLong("c_id", cId);
                    bundle.putString("course_id", mCourse);
                    bundle.putString("class_id", mClass);
                    bundle.putString("message", message);
                    Message sMessage = Message.obtain(null, PushService.SEND_MESSAGE);
                    sMessage.setData(bundle);
                    sMessage.replyTo = mMessenger;
                    mService.send(sMessage);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void postResponse(long cId, String time) {
        Long id;
        for (int i = mMessages.size() - 1; i >= 0; i--) {
            id = mMessages.get(i).getId();
            if (id.equals(cId)) {
                try {
                    time = dateFormat(time);
                } catch (ParseException e) {

                }

                mMessages.get(i).setTime(time);
                mAdapter.notifyItemChanged(i);
                break;
            }
        }
    }

    private void scrollToBottom() {
        mMessagesView.scrollToPosition(mAdapter.getItemCount() - 1);
    }

    class incomingHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            Bundle bundle;
            switch (message.what) {
                case PushService.NEW_MESSAGE:
                    bundle = message.getData();
                    String time;
                    try {
                        time = dateFormat(bundle.getString("time"));
                    } catch (ParseException e) {
                        time = "";
                    }
                    addMessage(0, bundle.getString("username"), bundle.getString("message"), time, 1);
                    break;
                case PushService.SENT_MESSAGE:
                    bundle = message.getData();
                    postResponse(bundle.getLong("c_id", 0), bundle.getString("time"));
                    break;
                default:
                    super.handleMessage(message);
            }
        }
    }
}
