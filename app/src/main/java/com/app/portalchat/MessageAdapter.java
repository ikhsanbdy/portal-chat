package com.app.portalchat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private List<ItemMessage> mMessages;
    private int[] mUsernameColors;

    public MessageAdapter(Context context, List<ItemMessage> messages) {
        mMessages = messages;
        mUsernameColors = context.getResources().getIntArray(R.array.username_colors);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = -1;
        switch (viewType) {
            case ItemMessage.TYPE_MESSAGE_SELF:
                layout = R.layout.item_message_right;
                break;
            case ItemMessage.TYPE_MESSAGE_OTHER:
                layout = R.layout.item_message_left;
                break;
        }

        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        ItemMessage message = mMessages.get(position);
        viewHolder.setMessage(message.getMessage());
        viewHolder.setUsername(message.getUsername());
        viewHolder.setTime(message.getTime());
        viewHolder.setIcon(message.getUsername());
        viewHolder.setSent(message.getSent());
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mMessages.get(position).getType();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mUsernameView;
        private TextView mMessageView;
        private TextView mTimeView;
        private TextView mIconView;
        private ImageView mSentView;
        //private View mIconShapeView;

        public ViewHolder(View itemView) {
            super(itemView);

            mUsernameView = (TextView) itemView.findViewById(R.id.username);
            mMessageView = (TextView) itemView.findViewById(R.id.message);
            mIconView = (TextView) itemView.findViewById(R.id.user_icon);
            mTimeView = (TextView) itemView.findViewById(R.id.message_time);
            mSentView = (ImageView) itemView.findViewById(R.id.message_sent);
            //mIconShapeView = itemView.findViewById(R.id.message_icon);
        }

        public void setUsername(String username) {
            if (null == mUsernameView) return;
            mUsernameView.setText(username);
            //int color = getUsernameColor(username);
            //mUsernameView.setTextColor(color);
            //setIconShapeColor(color);
        }

        public void setMessage(String message) {
            if (null == mMessageView) return;
            mMessageView.setText(message);
        }

        public void setIcon(String username) {
            if (null == mIconView || username.length() == 0) return;
            String initial = username.substring(0, 1);
            mIconView.setText(initial);
        }

        public void setTime(String time) {
            if (null == mTimeView) return;
            mTimeView.setText(time);
        }

        public void setSent(boolean sent) {
            if (null == mSentView) return;
            if (sent) {
                mSentView.setImageResource(R.mipmap.ic_message_ok);
            } else {
                mSentView.setImageResource(0);
            }
        }

        /*private void setIconShapeColor(int color) {
            if(null == mIconShapeView) return;
            GradientDrawable drawable = (GradientDrawable) mIconShapeView.getBackground();
            drawable.setColor(color);
        }

        private int getUsernameColor(String username) {
            int hash = 7;
            for (int i = 0, len = username.length(); i < len; i++) {
                hash = username.codePointAt(i) + (hash << 5) - hash;
            }
            int index = Math.abs(hash % mUsernameColors.length);
            return mUsernameColors[index];
        }*/
    }
}
