package com.app.portalchat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ikhsan on 3/5/2015.
 */
public class DBController extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "chat.db";

    public DBController(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        String query = "CREATE TABLE IF NOT EXISTS posts (id INTEGER PRIMARY KEY AUTOINCREMENT, s_id TEXT, course_id TEXT, class_id TEXT, username TEXT, message TEXT, time DATETIME DEFAULT NULL, read INTEGER)";
        database.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int currentVersion) {
        String query = "DROP TABLE IF EXISTS posts";
        database.execSQL(query);
        onCreate(database);
    }

    public long insertPost(JSONObject object, int read) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try {
            values.put("s_id", object.getString("_id"));
            values.put("course_id", object.getString("course_id"));
            values.put("class_id", object.getString("class_id"));
            values.put("username", object.getString("username"));
            values.put("time", object.getString("time"));
            values.put("message", object.getString("message"));
            values.put("read", read);
        } catch (JSONException e) {
            return 0;
        }

        long query = database.insert("posts", null, values);
        database.close();
        return query;
    }

    public int updateSentPost(long id, String sId, String time) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("s_id", sId);
        values.put("time", time);

        int query = database.update("posts", values, "id = ?", new String[]{String.valueOf(id)});
        database.close();

        return query;
    }

    public int updateReadPost(String courseId, String classId) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("read", 1);

        int query = database.update("posts", values, "course_id = ? AND class_id = ? AND read = ?", new String[]{courseId, classId, "0"});
        database.close();

        return query;
    }

    public void deleteUnsentPost(long id) {
        SQLiteDatabase database = this.getWritableDatabase();

        database.delete("posts", "id = ?", new String[]{String.valueOf(id)});
        database.close();
    }

    public ArrayList<HashMap<String, String>> getMessages(String courseId, String classId, int limit, int offset) {
        ArrayList<HashMap<String, String>> messageList;
        messageList = new ArrayList<HashMap<String, String>>();

        String selectQuery = "SELECT * FROM posts WHERE course_id = '" + courseId + "' AND class_id = '" + classId + "' ORDER BY id DESC LIMIT " + limit + " OFFSET " + offset;
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", cursor.getString(cursor.getColumnIndex("id")));
                map.put("username", cursor.getString(cursor.getColumnIndex("username")));
                map.put("message", cursor.getString(cursor.getColumnIndex("message")));
                map.put("time", cursor.getString(cursor.getColumnIndex("time")));
                map.put("read", cursor.getString(cursor.getColumnIndex("read")));
                messageList.add(map);
            } while (cursor.moveToNext());
        }

        return messageList;
    }

    public ArrayList<HashMap<String, String>> getUnreadMessages(String courseId, String classId) {
        ArrayList<HashMap<String, String>> messageList;
        messageList = new ArrayList<HashMap<String, String>>();

        String selectQuery = "SELECT * FROM posts WHERE course_id = '" + courseId + "' AND class_id = '" + classId + "' AND read = 0 ORDER BY id ASC";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", cursor.getString(cursor.getColumnIndex("id")));
                map.put("username", cursor.getString(cursor.getColumnIndex("username")));
                map.put("message", cursor.getString(cursor.getColumnIndex("message")));
                map.put("time", cursor.getString(cursor.getColumnIndex("time")));
                messageList.add(map);
            } while (cursor.moveToNext());
        }

        return messageList;
    }
}
