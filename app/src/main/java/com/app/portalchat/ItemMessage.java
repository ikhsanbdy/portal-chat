package com.app.portalchat;

public class ItemMessage {

    public static final int TYPE_MESSAGE_SELF = 0;
    public static final int TYPE_MESSAGE_OTHER = 1;

    private int mType;
    private long mId;
    private String mMessage;
    private String mUsername;
    private String mTime;

    private ItemMessage() {
    }

    public long getId() {
        return mId;
    }

    public int getType() {
        return mType;
    }

    public boolean getSent() {
        boolean sent = false;
        if (!mTime.equals("Sending") && !mTime.equals("") && !mTime.equals("Failed, tap to retry")) {
            sent = true;
        }
        return sent;
    }

    public String getMessage() {
        return mMessage;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getTime() {
        return mTime;
    }

    public void setTime(String time) {
        mTime = time;
    }

    public static class Builder {
        private final int mType;
        private long mId = 0;
        private String mUsername;
        private String mMessage;
        private String mTime;

        public Builder(int type) {
            mType = type;
        }

        public Builder username(String username) {
            mUsername = username;
            return this;
        }

        public Builder message(String message) {
            mMessage = message;
            return this;
        }

        public Builder id(long id) {
            mId = id;
            return this;
        }

        public Builder time(String time) {
            mTime = time;
            return this;
        }

        public ItemMessage build() {
            ItemMessage message = new ItemMessage();
            message.mType = mType;
            message.mId = mId;
            message.mUsername = mUsername;
            message.mMessage = mMessage;
            message.mTime = mTime;
            return message;
        }
    }
}
